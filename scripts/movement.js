var positionX;
var positionY;

function generateNewEndPoint() {
    var i = 0;
    while (i == 0) {
        var X = Math.floor(Math.random() * 20) + 1; 
        var Y = Math.floor(Math.random() * 20) + 1;

        if (maze[Y][X] == 0) { 
            i++;
        }
    }
    return [X, Y];
}

function getClickPosition() {
    if (game.input.activePointer.isDown) {
        positionX = Math.round((game.input.x - tileSize/2) / tileSize);
        positionY = Math.round((game.input.y - tileSize/2) / tileSize);
    }
}

function findPath(startPointX, startPointY, endPointX, endPointY) {
    var pathValue;
    easystar.findPath(startPointX, startPointY, endPointX, endPointY, myPath);
    easystar.calculate();

    function myPath(path) { 
        pathValue = path;
    }
    return pathValue;
}