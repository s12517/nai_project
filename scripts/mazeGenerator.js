var mazeGraphics;
var maze = [];
var width = 21;
var height = 25;
var tileSize = 26;


function generateMaze(){
    mazeGraphics = game.add.graphics(0, 0);
    var moves = [];
    for(var i = 0; i < height; i ++){
         maze[i] = [];
         for(var j = 0; j < width; j ++){
              maze[i][j] = 1;
         }
    }
    var X = 1;
    var Y = 1;
    var previousPossibleDirections = "";
    maze[X][Y] = 0;
    moves.push(Y + Y * width);
    while(moves.length){
         var possibleDirections = "";
         if(X+2 > 0 && X + 2 < height - 1 && maze[X + 2][Y] == 1){
              possibleDirections += "S";
         }
         if(X-2 > 0 && X - 2 < height - 1 && maze[X - 2][Y] == 1){
              possibleDirections += "N";
         }
         if(Y-2 > 0 && Y - 2 < width - 1 && maze[X][Y - 2] == 1){
              possibleDirections += "W";
         }
         if(Y+2 > 0 && Y + 2 < width - 1 && maze[X][Y + 2] == 1){
              possibleDirections += "E";
         }

        
      
         if (Math.random() > 0.85) { 
             if (X + 2 > 0 && X + 2 < height - 1 && maze[X + 2][Y] == 0) {
                 possibleDirections += "S";
             }
             if (X - 2 > 0 && X - 2 < height - 1 && maze[X - 2][Y] == 0) {
                 possibleDirections += "N";
             }
             if (Y - 2 > 0 && Y - 2 < width - 1 && maze[X][Y - 2] == 0) {
                 possibleDirections += "W";
             }
             if (Y + 2 > 0 && Y + 2 < width - 1 && maze[X][Y + 2] == 1) {
                 possibleDirections += "E";
             }
 
         }


         if(possibleDirections){

              var move = game.rnd.between(0, possibleDirections.length - 1);

                  var dig;
                 
                       dig=possibleDirections[move];
                   

              switch (dig){
                   case "N":
                        maze[X - 2][Y] = 0;
                        maze[X - 1][Y] = 0;
                        X -= 2;
                        break;
                   case "S":
                        maze[X + 2][Y] = 0;
                        maze[X + 1][Y] = 0;
                        X += 2;
                        break;
                   case "W":
                        maze[X][Y - 2] = 0;
                        maze[X][Y - 1] = 0;
                        Y -= 2;
                        break;
                   case "E":
                        maze[X][Y + 2]=0;
                        maze[X][Y + 1]=0;
                        Y += 2;
                        break;
              }
              previousPossibleDirections =dig;
              moves.push(Y + X * width);
         }
         else{
              var retreat = moves.pop();
              X = Math.floor(retreat / width);
              Y = retreat % width;
         }
    }


    drawMaze(X, Y);
}

function drawMaze(posX, posY){
    mazeGraphics.clear();
    for(i = 0; i < height; i ++){
         for(j = 0; j < width; j ++){
              if(maze[i][j] == 1){
                hedge = game.add.sprite(j *tileSize, i *tileSize, 'hedge');   
              }
         }
    }
    mazeGraphics.endFill();       
}